{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE TypeFamilies #-}

module Lib
  ( parseResourceURI
  ) where

import Data.Proxy (Proxy(..))
import Data.Semigroup ((<>))
import Data.String.Conversions (cs)
import Data.Text (Text, stripPrefix)
import GHC.TypeLits (KnownSymbol, symbolVal)
import Network.Google.Types (FromStream)
import Network.HTTP.Types (decodePathSegments)
import Network.URI (parseURI, uriPath)
import Servant.API ((:>), Capture, Get, QueryParam)
import Web.HttpApiData
       (FromHttpApiData, ToHttpApiData, parseUrlPiece)

class API s where
  type Fn s r :: *
  handle :: Proxy s -> [Text] -> Fn s r -> Either Text r

instance FromStream c a =>
         API (Get (c ': cs) a) where
  type Fn (Get (c ': cs) a) r = r
  handle _ _ = return

instance (KnownSymbol s, FromHttpApiData a, API api) =>
         API (Capture s a :> api) where
  type Fn (Capture s a :> api) r = a -> Fn api r
  handle _ [] _ = Left "Expecting another path segment"
  handle Proxy (seg:rest) f = do
    v <- parseUrlPiece seg
    handle (Proxy :: Proxy api) rest (f v)

instance (KnownSymbol s, API api) =>
         API (s :> api) where
  type Fn (s :> api) r = Fn api r
  handle _ [] _ = Left "Expecting another path segment"
  handle Proxy (seg:rest) f =
    if seg == cs (symbolVal (Proxy :: Proxy s))
      then handle (Proxy :: Proxy api) rest f
      else Left
             ("Expecting " <> cs (symbolVal (Proxy :: Proxy s)) <> ", got " <>
              seg)

instance (KnownSymbol s, ToHttpApiData a, API api) =>
         API (QueryParam s a :> api) where
  type Fn (QueryParam s a :> api) r = Fn api r
  handle _ = handle (Proxy :: Proxy api)

getPathSegmentsFromURI :: Text -> Maybe [Text]
getPathSegmentsFromURI uri =
  (fmap (decodePathSegments . cs . uriPath) . parseURI) (cs uri)

parseResourceURI
  :: API api
  => Proxy api -> Text -> Fn api r -> Either Text r
parseResourceURI p uri f =
  case getPathSegmentsFromURI uri of
    Just segments -> handle p segments f
    Nothing -> Left ("error parsing uri " <> uri)
